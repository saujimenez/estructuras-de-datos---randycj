/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    Proyecto Programado #1
    Estudiantes: Randy Conejo Juárez
                 Kevin Zumbado Cruz
                 Saúl Jiménez González
**********************************************************************/

//Declaración de Macros
#define RANGO 52 // de 1 a ..

//Declaración de estructuras
typedef struct Carta{
    int valor_total;
    int valor;
    struct Carta *sig;
    struct Carta *ant;
}Carta;

typedef struct CartaActual{
    Carta *cabeza;
    Carta *actual;
    int posicion_actual;
}CartaActual;

//Declaración de Funciones
/*-----------------------------------------------------------------------
    main
    Entradas: Sin parámetros de entrada
    Salidas: Un número entero indicando el código de salida del programa
    Funcionamiento: Función main, la que inicia el programa, en este caso, crea cartas y llama al menú
                    Se reserva espacio para la carta que se va a crear y se llama al método crear_cartas
                    Por último, se llama a menu, que va a interactuar con el usuario.
        - 
-----------------------------------------------------------------------*/
void main();
/*-----------------------------------------------------------------------
    crear_cartas
    Entradas: Sin parámetros de entrada
    Salidas: void, sin salidas
    Funcionamiento: Crea todas las cartas
                    Se crea un contador para el valor ordinario y otro contador para asignar el valor total
                    Mientras que el contador no sea 52 se crean las cartas con su valor total respectivo
                    Si el contador es igual a 52, el siguiente de la última carta será NULL, indicando el final de la baraja
        - 
-----------------------------------------------------------------------*/
void crear_cartas();
/*-----------------------------------------------------------------------
    carta_nueva
    Entradas: Un entero indicando el valor total y otro entero indicando el valor ordinario de la carta a crear
    Salidas: Un puntero con tipo Carta, indicando la nueva carta creada
    Funcionamiento: Crea cada carta por individual, se complementa con crear_cartas
                    nueva_carta se crea como NULL, se asigna espacio para la carta
                    se asigna el valor total y el valor ordinario
                    Finalmente, se valida que si es la pimera carta, la anterior es NULL y se procede a retornar nueva_carta
        - 
-----------------------------------------------------------------------*/
Carta *carta_nueva(int valor_total, int valor);
/*-----------------------------------------------------------------------
    imprimir_cartas
    Entradas: Sin parámetros entrada
    Salidas: void, sin salidas
    Funcionamiento: Sirve para imprimir en pantalla los valores de cada carta
                    Se crea un puntero temporal, se reserva espacio para una nueva carta y se iguala a temporal
                    temporal se convierte en la carta actual a imprimir
                    Mientras que temporal no sea igual a NULL, se imprime, en orden, el valor y el valor total de cada carta
        - 
-----------------------------------------------------------------------*/
void imprimir_cartas();
/*-----------------------------------------------------------------------
    mostrar_actual
    Entradas: Un puntero tipo carta apuntando a la carta actual
    Salidas: void, sin salidas
    Funcionamiento: Muestra en pantalla la carta actual
                    Se definen las posiciones como los valores totales
                    Valida las posiciones de las cartas especiales como J,Q,K para imprimir la carta sin el valor numérico
                    En caso de no ser una de las especiales, se imprime con el valor ordinario
                    Se repite el proceso con cada tipo de carta
        - 
-----------------------------------------------------------------------*/
void mostrar_actual(Carta *actual);
/*-----------------------------------------------------------------------
    carta_siguiente
    Entradas: Sin parámetros de entrada
    Salidas: void, sin salidas
    Funcionamiento: Sirve para mostrar la carta siguiente a la carta actual de la baraja
                    Iguala el puntero de la carta actual al siguiente de la actual y suma la posición un elemento
        - 
-----------------------------------------------------------------------*/
void carta_siguiente();
/*-----------------------------------------------------------------------
    carta_anterior
    Entradas: Sin parámetros de entrada
    Salidas: void, sin salidas
    Funcionamiento: Sirve para mostrar en pantalla la carta anterior a la actual de la baraja
                    Iguala el puntero de la carta actual al de la anterior y resta la posición un elemento
        - 
-----------------------------------------------------------------------*/
void carta_anterior();
/*-----------------------------------------------------------------------
    barajar
    Entradas: Sin parámetros de entrada
    Salidas: void, sin salidas
    Funcionamiento: Sirve para hacer movimientos en la baraja simulando la acción de revolver el mazo
                    Se hace una lista con los valores de la baraja
                    Se consigue un numero aleatorio entre los valores de la baraja
                    La carta con el valor igual al número se convierte en la cabeza si es el primer número elegido
                    Se valida la familia de la carta dependiendo de su valor para asignarselo a la carta que se acaba de elegir
                    Se repite el proceso conviertendo temporal en su siguiente hasta que el indice "i" sea igual al rango de la lista
        - 
-----------------------------------------------------------------------*/
void barajar();
/*-----------------------------------------------------------------------
    menu
    Entradas: Sin parámetros de entrada
    Salidas: void, no hay salidas
    Funcionamiento: Es el menú con el que interactúa el usuario
                    Se dan las opciones al usuario
                    1. Mostrar carta actual/ muestra la carta actual
                    2. Mostrar Siguiente Carta/ muestra la carta siguiente a la actual
                    3. Mostrar Carta Anterior/ mustra la carta anterior a la actual
                    4. Mostrar toda la baraja/ muestra la baraja entera en su estado actual
                    5. Ordenar Baraja/ ordena la baraja de vuelta a su estado original
                    6. Barajar/ desordena la baraja con movimientos aleatorios
                    0. Salir/ termina la ejecución del programa
        - 
-----------------------------------------------------------------------*/
void menu();

/*-----------------------------------------------------------------------
    sacarPivote
    Entradas: La lista con los valores de las cartas, un entero izq,un entero der
    Salidas: Un entero indicando el pivote
    Funcionamiento: Sirve para tomar el pivote de la lista para hacer quicksort
                    El pivote es igual a izquierda, el valor del pivote es igual al de la lista con el mismo valor, indice i igual a izquierda más uno
                    Si el elemento i de la lista es menor al valor del pivote, se suma pivote
                    auxiliar es el elmento i, el elemento es igual a la lista en el elemento pivote, ese elemento es aux
                    auxiliar es igual al elemento izquierdo de la lista e igual al elemento pivote, se retorna pivote
        - 
-----------------------------------------------------------------------*/
int sacarPivote();

/*-----------------------------------------------------------------------
    Quicksort
    Entradas: La lista con los valores de las cartas, un entero izquierdo y un entero derecho
    Salidas: void, sin salidas
    Funcionamiento: Sirve para llevar a cabo el proceso de quicksort
                    Se define un entero pivote
                    Si izq es menor que der, se llama a sacarPivote
                    Se llama a si misma, una vez con derecha menos uno y otra con izquierda más uno
        - 
-----------------------------------------------------------------------*/
void Quicksort();

/*-----------------------------------------------------------------------
    ordenar
    Entradas: Lista con los valores de las cartas
    Salidas: void, sin salidas
    Funcionamiento: Sirve para ordenar con quicksort
                    Se define un entero que indique la familia de la carta
                    Se llama a Quicksort con el rango de la lista izquierdo y derecho
                    Se iguala temporal a la cabeza de la baraja
                    Se valida la familia de cada valor
                    El valor total de la carta es igual al elemento de la lista en el índice
                    El valor ordinario de la carta es igual al elemento en indice menos el valor de la familia
                    Temporal se iguala al siguiente hasta que el indice iguale al numero de cartas
        - 
-----------------------------------------------------------------------*/
void ordenar();
