#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct Carta{
    int valor_total;
    int valor;
    struct Carta *sig;
    struct Carta *ant;
}Carta;

typedef struct CartaActual{
    Carta *cabeza;
    Carta *actual;
    int posicion_actual;
}CartaActual;

//Definición de estructuras globales a usar
CartaActual *carta_actual = NULL;
//Definicion de Funciones
Carta *carta_nueva(int valor_total, int valor);
void crear_cartas();
void imprimir_cartas();
void menu();
void carta_siguiente();
void carta_anterior();

int main(){
    
    carta_actual = malloc(sizeof(CartaActual));
    crear_cartas();
    //imprimir_cartas();
    menu();
    
    return 0;
}


void crear_cartas(){
    int contador = 1;
    int contador_auxiliar = 1;
    
    Carta *temporal = NULL;
    Carta *nueva_carta = NULL;
    nueva_carta = malloc(sizeof(Carta));
    temporal = malloc(sizeof(Carta));
    
    while (contador <= 52){
        
        while (contador_auxiliar <= 13){//Se usan dos contadores, uno para el valor y otro para el valor total.
        
            nueva_carta = carta_nueva(contador, contador_auxiliar);//crea una variable de tipo carta
            if (contador == 1){
                temporal = nueva_carta;
                contador ++;
                contador_auxiliar ++;
                carta_actual->actual = nueva_carta;
                carta_actual->cabeza = nueva_carta;
                carta_actual->posicion_actual = 0;
            }
            else if (contador==52){
                nueva_carta->sig=NULL;
                temporal->sig = nueva_carta;
                nueva_carta->ant = temporal;
                contador ++;
                contador_auxiliar ++; 
            }
            else{
                temporal->sig = nueva_carta;
                nueva_carta->ant = temporal;
                temporal = nueva_carta;
                contador ++;
                contador_auxiliar ++;
            }
        }
    contador_auxiliar = 1;
    }
           
}

            
Carta *carta_nueva(int valor_total, int valor){
    Carta *nueva_carta = NULL;
    
    nueva_carta = malloc(sizeof(Carta));
    nueva_carta->valor_total = valor_total;
    nueva_carta->valor = valor;
    if (valor_total == 1){
        nueva_carta->ant = NULL;
    }
    return nueva_carta;
}
    
    
void imprimir_cartas(){
    Carta *temporal = NULL;
    temporal = malloc(sizeof(Carta));
    temporal = carta_actual->actual;
    
    while (temporal != NULL){
        printf("Valor : %d  Valor Total: %d\n", temporal->valor, temporal->valor_total);
        temporal = temporal->sig;
    }
    return;
}

void mostrar_actual(Carta *actual){

    int posicion = actual->valor_total;
    
    if (posicion <=13){
        if (actual->valor == 1){
            printf(".------.\n|A .   |\n| / \\  |\n|(_,_) |\n|  I  A|\n'------'\n");
        }
        else if (actual->valor == 11){
            printf(".------.\n|J .   |\n| / \\  |\n|(_,_) |\n|  I  J|\n'------'\n");
        }
        else if(actual->valor == 12){
            printf(".------.\n|Q .   |\n| / \\  |\n|(_,_) |\n|  I  Q|\n'------'\n");
        }
        else if(actual->valor == 13){
            printf(".------.\n|K .   |\n| / \\  |\n|(_,_) |\n|  I  K|\n'------'\n");
        }
        else if(actual->valor == 10){
            printf(".------.\n|10.   |\n| / \\  |\n|(_,_) |\n|  I 10|\n'------'\n");
        }
        else {
            printf(".------.\n|%d .   |\n| / \\  |\n|(_,_) |\n|  I  %d|\n'------'\n", actual->valor, actual->valor);
        }
    }
    
    else if(posicion <= 26){
        if (actual->valor == 1){
            printf(".------.\n|A_  _ |\n|( \\/ )|\n| \\  / |\n|  \\/ A|\n'------'\n");
        }
        else if (actual->valor == 11){
            printf(".------.\n|J_  _ |\n|( \\/ )|\n| \\  / |\n|  \\/ J|\n'------'\n");
        }
        else if(actual->valor == 12){
            printf(".------.\n|Q_  _ |\n|( \\/ )|\n| \\  / |\n|  \\/ Q|\n'------'\n");
        }
        else if(actual->valor == 13){
            printf(".------.\n|K_  _ |\n|( \\/ )|\n| \\  / |\n|  \\/ K|\n'------'\n");
        }
        else if(actual->valor == 10){
            printf(".-------.\n|10_  _ |\n| ( \\/ )|\n|  \\  / |\n|   \\/10|\n'-------'\n");
        }
        else {
            printf(".------.\n|%d_  _ |\n|( \\/ )|\n| \\  / |\n|  \\/ %d|\n'------'\n", actual->valor, actual->valor);
        }
    }
        
    else if (posicion <= 39){
        if (actual->valor == 1){
            printf(".------.\n|A /\\  |\n| /  \\ |\n| \\  / |\n|  \\/ A|\n'------'\n");
        }
        else if (actual->valor == 11){
            printf(".------.\n|J /\\  |\n| /  \\ |\n| \\  / |\n|  \\/ J|\n'------'\n");
        }
        else if (actual->valor == 12){
            printf(".------.\n|Q /\\  |\n| /  \\ |\n| \\  / |\n|  \\/ Q|\n'------'\n");
        }
        else if (actual->valor == 13){
            printf(".------.\n|K /\\  |\n| /  \\ |\n| \\  / |\n|  \\/ K|\n'------'\n");
        }
        else if (actual->valor == 10){
            printf(".------.\n|10/\\  |\n| /  \\ |\n| \\  / |\n|  \\/10|\n'------'\n");
        }
        else {
            printf(".------.\n|%d /\\  |\n| /  \\ |\n| \\  / |\n|  \\/ %d|\n'------'\n", actual->valor, actual->valor);
        }
    }
        
    else {
        if (actual->valor == 1){
            printf(".------.\n|A _   |\n| ( )  |\n|(_x_) |\n|  Y  A|\n'------'\n");
        }
        else if (actual->valor == 11){
            printf(".------.\n|J _   |\n| ( )  |\n|(_x_) |\n|  Y  J|\n'------'\n");
        }
        else if (actual->valor == 12){
            printf(".------.\n|Q _   |\n| ( )  |\n|(_x_) |\n|  Y  Q|\n'------'\n");
        }
        else if (actual->valor == 13){
            printf(".------.\n|K _   |\n| ( )  |\n|(_x_) |\n|  Y  K|\n'------'\n");
        }
        else if (actual->valor == 10){
            printf(".------.\n|10_   |\n| ( )  |\n|(_x_) |\n|  Y 10|\n'------'\n");
        }
        else {
            printf(".------.\n|%d _   |\n| ( )  |\n|(_x_) |\n|  Y  %d|\n'------'\n", actual->valor, actual->valor);
        }
    }

    
    
    
}

void menu(){

    int contador_total = 0;
    int eleccion;
    Carta *temporal = NULL;
    temporal = malloc(sizeof(Carta));
    
    do{
        printf("\nMenú Principal\n");
        printf("1. Mostrar Carta Actual\n");
        printf("2. Mostrar Siguiente Carta\n");
        printf("3. Mostrar Carta Anterior\n");
        printf("4. Mostrar toda la baraja\n");
        printf("5. Ordenar Baraja\n");
        printf("6. Barajar\n");
        printf("0. Salir\n");
        printf("Ingrese su elección: ");
        scanf("%d", &eleccion);
        
        switch (eleccion){
            
            case 1:
                mostrar_actual(carta_actual->actual);
                break;
            
            case 2:
                if (carta_actual->posicion_actual == 51){
                    printf("\nLlegó al final de la baraja\n");
                }
                else{
                    carta_siguiente();
                    mostrar_actual(carta_actual->actual);
                }
                break;
            
            case 3:
                if (carta_actual->posicion_actual == 0){
                    printf("\nLlegó al inicio de la baraja\n");
                }
                else{
                    carta_anterior();
                    mostrar_actual(carta_actual->actual);
                }
                break;
            
            case 4: 
                temporal = carta_actual->cabeza;
                while (temporal != NULL) {
                    mostrar_actual(temporal);
                    temporal = temporal->sig;
                }
                    contador_total = 0;
                    break;

        }
    }while(eleccion != 0);
}

void carta_siguiente(){
    carta_actual->actual = carta_actual->actual->sig;
    carta_actual->posicion_actual ++;
}

void carta_anterior(){
    carta_actual->actual = carta_actual->actual->ant;
    carta_actual->posicion_actual --;
}
