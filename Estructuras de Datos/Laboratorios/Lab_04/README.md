**Laboratorio #4.**

Lea y comprenda el código ejemplo entregado por la profesora, debe hacer lo siguiente:


1. Ejecutar el código, realizando las distintas funciones disponibles con datos de prueba. Debe evidenciar si ejecución con al menos 10 capturas de pantalla documentadas en el blog del curso.


2. Abrir los archivos y documentar las declaraciones de funciones en el archivo de encabezado (lista_estudiantes.h), a partir de la implementación disponible en el archivo de código fuente.


3. Extender el código para que la lista sea no sea simple, si no circular.
