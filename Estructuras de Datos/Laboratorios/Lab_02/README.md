**Laboratorio #2.**

Utilice como base el código del laboratorio #1 y modifiquelo para que opere sobre una lista enlazada simple (dinámica) de tamaño n. 
Esta vez se le deberá solicitar al usuario la cantidad de estudiantes que desee ingresar.
Después de ingresar los estudiantes, el programa seguirá el siguiente comportamiento.


PC$ ¿Que posición desea validar?

	> 2


PC$ ¿Cuál es el carnet del estudiante en la posición 2?

	> 201925317



*Si el carnet está correcto*

	PC$ El carnet ingresado es correcto.


*Si el carnet está erroneo*

	PC$ El carnet ingresado no corresponde con la posición 2.
