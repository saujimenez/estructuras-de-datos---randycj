**Laboratorio 6**


Estudiantes: -Randy Conejo Juárez. 2019066448

	     -Kevin Zumbado Cruz. 2019258634


En este laboratorio diseñaremos un controlador de dispositivos inteligentes, que permite accionar sobre los dispositivos de una casa inteligente.


- La casa tendrá​n ​cantidad de ​habitaciones​ ,con ​m ​​dispositivos inteligentes​.


- Cada dispositivo podrá ser encendido o apagado desde el controlador.


- Algunos dispositivos tendrán comportamientos particulares, por ejemplo, en una lavadora se pueden seleccionar varios ciclos de lavado, o se puede reiniciar una computadora


- El controlador también permite encender o apagar todos los dispositivos de una habitación en particular.
