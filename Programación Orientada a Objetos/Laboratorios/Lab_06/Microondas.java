/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiantes: 
    -Kevin Zumbado Cruz (2019258634)
    -Randy Conejo Juarez (2019066448)
    
    Laboratorio 6
    Para probar este código en línea, visite el siguiente link: https://repl.it/@RandyCJ/Lab-06
**********************************************************************/
public class Microondas extends Dispositivo{

  private String modo;

  
  public Microondas(){
    super("Microondas");
    modo="Normal";
  }

  public void cambiarModo(){
    switch(modo){
    case "Normal":
      modo="Palomitas";
    break;
    case "Palomitas":
      modo="Normal";
    break;
    }
  }

  public String toString(){
    if (super.getEstado()=="Encendido"){
      return "\nNombre: "+ super.getAparato() +"\nEstado: "+ super.getEstado()+ "\nModo: "+ modo;
    }else{
      return "\nNombre: "+ super.getAparato() +"\nEstado: "+ super.getEstado();
    }
  }
}