/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiantes: 
    -Kevin Zumbado Cruz (2019258634)
    -Randy Conejo Juarez (2019066448)
    
    Laboratorio 6
    Para probar este código en línea, visite el siguiente link: https://repl.it/@RandyCJ/Lab-06
**********************************************************************/
import java.util.ArrayList;

public class Controlador{

  public Controlador(){
  }

  //Los métodos de esta clase comprueban de que clase es el objeto que le están mandando como parámetro para así ejecutar una acción diferente dependiendo de la clase
  public void encender(Object o){
    if (o instanceof Dispositivo){//Si es un dispositivo, solamente enciende el dispositivo
      ((Dispositivo) o).encender();
      System.out.println("\nSe ha encendido un dispositivo: " + ((Dispositivo) o).getAparato());
    }
    if (o instanceof Habitacion){//Si es una habitación, enciende todos los aparatos de esa habitación
      
      ArrayList<Dispositivo> tmp;
      tmp = ((Habitacion) o).getDispositivos();

      int contador = ((Habitacion) o).getCantidad();

      for (int i=0; i < ((Habitacion) o).getCantidad(); i++){
        if (tmp.get(i).getEstado()=="Encendido"){
          contador--;
        }
        tmp.get(i).encender();
      }
      System.out.println("\nSe han encendido " + contador + " dispositivo(s)");
    }
    if (o instanceof Casa){//Si es la casa, enciende todos los aparatos de la casa

      Casa casa =((Casa) o);
      
      ArrayList<Habitacion> tmp= casa.getHabitaciones();
      
      for (int i=0; i<tmp.size();i++){
      
        ArrayList<Dispositivo> aux=tmp.get(i).getDispositivos();
      
        int contador = aux.size();
      
        for (int j=0;j<aux.size();j++){
          if (aux.get(j).getEstado()=="Encendido"){
            contador--;
          }
          aux.get(j).encender();
        }
        System.out.println("\nSe han encendido " + contador + " dispositivo(s) en la habitación: "+tmp.get(i).getNombre());
      }
    }
  }

  public void imprimeObjetos(Object o){
    if (o instanceof Casa){
      ((Casa) o).imprimeAparatos();
    }
        if (o instanceof Habitacion){
      ((Habitacion) o).imprimeAparatos();
    }
  }
  
  public void apagar(Object o){
    
    if (o instanceof Dispositivo){
      ((Dispositivo) o).apagar();
      System.out.println("\nSe ha apagado un dispositivo: " + ((Dispositivo) o).getAparato());
    }

    if (o instanceof Habitacion){
    
      ArrayList<Dispositivo> tmp;
      tmp = ((Habitacion) o).getDispositivos();

      int contador = ((Habitacion) o).getCantidad();

      for (int i=0; i < ((Habitacion) o).getCantidad(); i++){
        if (tmp.get(i).getEstado()=="Apagado"){
          contador--;
        }
        tmp.get(i).apagar();
      }
      System.out.println("\nSe han apagado " + contador + " dispositivo(s)");
    }

    if (o instanceof Casa){

      Casa casa =((Casa) o);
      
      ArrayList<Habitacion> tmp= casa.getHabitaciones();
      
      for (int i=0; i<tmp.size();i++){
      
        ArrayList<Dispositivo> aux=tmp.get(i).getDispositivos();
      
        int contador = aux.size();
      
        for (int j=0;j<aux.size();j++){
          if (aux.get(j).getEstado()=="Apagado"){
            contador--;
          }
          aux.get(j).apagar();
        }
        System.out.println("\nSe han apagado " + contador + " dispositivo(s) en la habitación: "+tmp.get(i).getNombre());
      }
    }
  }
}