/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiantes: 
    -Kevin Zumbado Cruz (2019258634)
    -Randy Conejo Juarez (2019066448)
    
    Laboratorio 6
    Para probar este código en línea, visite el siguiente link: https://repl.it/@RandyCJ/Lab-06
**********************************************************************/
import java.util.ArrayList;

public class Habitacion{
  
  private String nombre;
  private ArrayList<Dispositivo> dispositivos;
 
  public Habitacion(String nombre){
    this.nombre = nombre;
    dispositivos = new ArrayList<Dispositivo>();
  }

  public void agregarDispositivo(Dispositivo nuevo){
    this.dispositivos.add(nuevo);
  }

  public void imprimeAparatos(){
    System.out.println("_____________________________________________");
    System.out.println("\nLista de Dispositivos de " + getNombre() + "\n");
    for (int i=0; i<dispositivos.size(); i++){
      System.out.println(dispositivos.get(i).toString() + "\n");
    }
    System.out.println("_____________________________________________");
  }

  public ArrayList<Dispositivo> getDispositivos(){
    return dispositivos;
  }

  public int getCantidad(){
    return this.dispositivos.size();
  }

  public String getNombre(){
    return this.nombre;
  }
  
}