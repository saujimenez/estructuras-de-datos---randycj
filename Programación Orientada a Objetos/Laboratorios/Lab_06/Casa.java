/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiantes: 
    -Kevin Zumbado Cruz (2019258634)
    -Randy Conejo Juarez (2019066448)
    
    Laboratorio 6
    Para probar este código en línea, visite el siguiente link: https://repl.it/@RandyCJ/Lab-06
**********************************************************************/
import java.util.ArrayList;

public class Casa{
  private ArrayList<Habitacion> habitaciones;

  public Casa(){
    habitaciones = new ArrayList<Habitacion>();
  }

  public ArrayList<Habitacion> getHabitaciones(){
    return habitaciones;
  }

  public void agregarHabitacion(Habitacion nuevaHabitacion){
    habitaciones.add(nuevaHabitacion);
  }

  public void imprimeAparatos(){

    for (int i=0; i<habitaciones.size(); i++){
      System.out.println("_____________________________________________");
      System.out.println("\nLista de Dispositivos de " + habitaciones.get(i).getNombre() + "\n");
      ArrayList<Dispositivo> dispositivos= habitaciones.get(i).getDispositivos();
      for (int j=0; j<dispositivos.size(); j++){
        System.out.println(dispositivos.get(j).toString() + "\n");
      }
    }
    System.out.println("_____________________________________________");
  }

}