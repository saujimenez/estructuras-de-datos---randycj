import java.util.Scanner;

class Main{
    public static void main(String[] args){
        
        PilaTEC ListaEnteros = new PilaTEC();
        Scanner entero = new Scanner(System.in);
        int entero_ingresado;
        
        for (int i = 0; i<5; i++){
            System.out.print("Ingrese un número: ");
            entero_ingresado = entero.nextInt();
            ListaEnteros.push(entero_ingresado);
            System.out.println("");
        }
        
        System.out.println("Lista almacenada");
        
        ListaEnteros.printList();
    }
}
