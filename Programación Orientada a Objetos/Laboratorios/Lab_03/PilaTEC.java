import java.util.ArrayList;

public class PilaTEC extends ArrayList{

    
    public PilaTEC(){
        super();
    }
    
    public boolean isEmpty(){
      return super.isEmpty();
    }
    
    public int getSize(){
        return super.size();
    }
    
    public Object peek(){
        return super.get(getSize()-1);
    }
    
    public Object pop(){
        Object LastObject = super.get(getSize()-1);
        super.remove(getSize()-1);
        return LastObject;
    }
    
    public void push(Object nuevoObjeto){
        super.add(nuevoObjeto);
    }
    
    @Override
    public String toString(){
        return "pila: " + super.toString();
    }
    
    public void printList(){
        int cantidad = getSize();
        for (int i = 0; i<cantidad; i++){
        System.out.print(pop() + "  ");
        } 
    }
}
